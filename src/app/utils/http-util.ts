import { HttpHeaders } from '@angular/common/http';
import * as lodash from 'lodash';
import { throwError } from 'rxjs';

export class HttpUtil {

    constructor() { }

    /**
     * Build the Request Header
     * @param headerParams request[params]
     * @param queryParams request[query]
     */
    static getHttpOptions(headerParams?: any, queryParams?: any): Object {

        let headerPayload = {};

        // Add Content Type
        headerPayload['Content-Type'] = 'application/json';

        // Add Header Params to request
        if (headerParams) {
            lodash.forEach(Object.getOwnPropertyNames(headerParams), function (value, index) {
                let objKey = value;
                let objValue = headerParams[objKey];
                headerPayload[objKey] = objValue;
            });
        }

        // Add Query Params to request
        let queryPayload = {};
        if (queryParams) {
            lodash.forEach(Object.getOwnPropertyNames(queryParams), function (value, index) {
                let objKey = value;
                let objValue = queryParams[objKey];
                queryPayload[objKey] = objValue;
            });
        }

        // Add Headers to HttpHeaders
        let httpOptions = {
            headers: new HttpHeaders(headerPayload),
            observe: 'response' as 'response'
        };

        if (queryParams) { httpOptions['params'] = queryParams; }

        return httpOptions;
    }

    /**
     * Extract body from Response
     */
    static extractData(res: Response) {
        const body = res ? res.body : {};
        return body;
    }

    static handleErrorObservableReturnHttpCode(error: any) {
      console.log('HttpUtil | Error on observable = ' + error.status);
      return throwError(error.status);
  }

}
