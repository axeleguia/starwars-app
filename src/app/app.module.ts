import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FilmComponent } from './components/film/film.component';
import { FilmService } from './services/film/film.service';
import { HttpClientModule } from '@angular/common/http';
import { CharacterComponent } from './components/character/character.component';
import { CharacterService } from './services/character/character.service';
import { MainComponent } from './main/main.component';

@NgModule({
  declarations: [
    AppComponent,
    FilmComponent,
    CharacterComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    FilmService,
    CharacterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
