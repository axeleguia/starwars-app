import { Component, OnInit } from '@angular/core';
import { FilmModel } from 'src/app/models/film';
import { FilmService } from 'src/app/services/film/film.service';
import { Router } from '@angular/router';
import { CharacterService } from 'src/app/services/character/character.service';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.scss']
})
export class FilmComponent implements OnInit {

  filmList: FilmModel[];

  constructor(
    private characterService: CharacterService,
    private filmService: FilmService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getFilms();
  }

  getFilms() {
    this.filmService.getFilms().subscribe(
      (response: any) => {

        this.filmList = Object.assign(response, FilmModel);

      },
      (error: any) => {
        console.log(error);
      }
    )
  }

  viewDetails() {
    this.router.navigate(['characters']);
  }

  viewMain() {
    this.router.navigate(['main']);
  }

}
