import { Component, OnInit } from '@angular/core';
import { CharacterModel } from 'src/app/models/character';

import { Router } from '@angular/router';
import { CharacterService } from 'src/app/services/character/character.service';
import { FilmService } from 'src/app/services/film/film.service';
import * as lodash from 'lodash';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {

  characterList: CharacterModel[];

  constructor(
    private characterService: CharacterService,
    private filmService: FilmService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getCharacters();
  }

  getCharacters() {
    this.characterService.getFilms().subscribe(
      (response: any) => {
        this.characterList = Object.assign(response, CharacterModel);
        this.characterList= lodash.sortBy(this.characterList, ['name']);
      },
      (error: any) => {
        console.log(error);
      }
    )
  }

  viewDetails() {
    this.router.navigate(['characters']);
  }

  viewMain() {
    this.router.navigate(['main']);
  }


}
