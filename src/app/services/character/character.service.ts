import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs'
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { HttpUtil } from 'src/app/utils/http-util';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Call API Rest to list people
   */
  getFilms(): Observable<any> {
    const params = {};
    const url = environment.api_url +'/personajes';
    return this.http.get(url, HttpUtil.getHttpOptions( null, params)).pipe(
			map(HttpUtil.extractData),
			catchError(HttpUtil.handleErrorObservableReturnHttpCode)
		);
  }

}
